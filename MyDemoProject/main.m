//
//  main.m
//  MyDemoProject
//
//  Created by Mradul Mathur on 02/08/16.
//  Copyright © 2016 Mradul Mathur. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
